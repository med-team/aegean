aegean (0.16.0+dfsg-4) unstable; urgency=medium

  * Remove explicit runtime dependency on libgenometools0.
    Closes: #1068178
  * Add d/copyright information for debian/ directory.
  * Bump Standards-Version.
  * Add Rules-Requires-Root: no.
  * Update debhelper compat level to 13.
  * Only list files not installed.
  * Adjust renamed lintian tag overrides.
  * Bump watchfile version to 4.

 -- Sascha Steinbiss <satta@debian.org>  Mon, 01 Apr 2024 19:49:51 +0200

aegean (0.16.0+dfsg-3) unstable; urgency=medium

  * Ensure multiple builds can be done in same directory.
    Done by ensuring the directory is cleaned correctly.
    Closes: #1043943

 -- Sascha Steinbiss <satta@debian.org>  Wed, 15 Nov 2023 11:32:13 +0100

aegean (0.16.0+dfsg-2) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Move source package lintian overrides to debian/source.

  [ Andreas Tille ]
  * Standards-Version: 4.4.0
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test target
  * Do not parse d/changelog
  * Use 2to3 to convert from Python2 to Python3
    Closes: #936109
  * Drop git from Build-Depends
  * Drop redundant debian/README.source
  * debhelper-compat 12
  * Use secure URI in Homepage field.
  * Set fields Upstream-Contact in debian/copyright.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.

 -- Sascha Steinbiss <satta@debian.org>  Mon, 09 Sep 2019 18:52:15 +0200

aegean (0.16.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update watchfile to reflect move to BrendelGroup org.

 -- Sascha Steinbiss <satta@debian.org>  Fri, 05 Oct 2018 18:03:42 +0200

aegean (0.15.2+dfsg-2) unstable; urgency=medium

  [ Andreas Tille ]
  * Fix and adjust upstream metadata.

  [ Steffen Moeller ]
  * Fix EDAM annotations.

  [ Sascha Steinbiss ]
  * Update Uploader email address.
  * Use debhelper 11.
  * Use secure d/copyright format link.
  * Bump Standards-Version.
  * Update Vcs-* fields with Salsa addresses.
  * Replace priority extra with optional.
  * Remove unnecessary Testsuite field.
  * Drop unused patch files.
  * Drop -dbg package.
  * Fix naming of lintian override.

 -- Sascha Steinbiss <satta@debian.org>  Wed, 04 Jul 2018 18:20:29 +0200

aegean (0.15.2+dfsg-1) unstable; urgency=medium

  * New upstream version.
  * Drop patches applied upstream.
  * Add override for pedantic Lintian msg.
  * d/watch: make watchfile more robust
  * d/rules: drop unneeded get-orig-source target
  * d/rules: enable full hardening
  * d/rules: use DEB_TARGET_ARCH_BITS
  * d/control: bump standards version

 -- Sascha Steinbiss <sascha@steinbiss.name>  Sun, 20 Mar 2016 11:31:05 +0000

aegean (0.15.1+dfsg-3) unstable; urgency=low

  [ Jon Ison, Steffen Moeller, Sascha Steinbiss ]
  * Added EDAM annotation.

  [ Sascha Steinbiss ]
  * Migrate package to Git

 -- Sascha Steinbiss <sascha@steinbiss.name>  Sun, 07 Feb 2016 08:55:37 +0000

aegean (0.15.1+dfsg-2) unstable; urgency=medium

  * Remove broken archs, as revealed by newly added tests.

 -- Sascha Steinbiss <sascha@steinbiss.name>  Sat, 09 Jan 2016 18:28:59 +0000

aegean (0.15.1+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Sascha Steinbiss <sascha@steinbiss.name>  Sat, 09 Jan 2016 14:16:31 +0000

aegean (0.14.1+dfsg2-2) unstable; urgency=low

  * d/rules: build w/o parallelism, set LC_ALL
  * patch Makefile to remove nondeterminism

 -- Sascha Steinbiss <sascha@steinbiss.name>  Sun, 29 Nov 2015 19:19:01 +0000

aegean (0.14.1+dfsg2-1) unstable; urgency=medium

  * Exclude DataTables JS source, add Depends to new package
    Closes: #798900
  * Remove now redundant Lintian override

 -- Sascha Steinbiss <sascha@steinbiss.name>  Fri, 27 Nov 2015 22:35:59 +0000

aegean (0.14.1+dfsg-1) unstable; urgency=low

  * New upstream release.

 -- Sascha Steinbiss <sascha@steinbiss.name>  Sat, 10 Oct 2015 09:43:34 +0000

aegean (0.13.0+dfsg-1) unstable; urgency=low

  * New upstream release.

 -- Sascha Steinbiss <sascha@steinbiss.name>  Tue, 28 Jul 2015 08:50:09 +0000

aegean (0.10.2+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #769162)

 -- Sascha Steinbiss <sascha@steinbiss.name>  Tue, 11 Nov 2014 11:14:11 +0000
